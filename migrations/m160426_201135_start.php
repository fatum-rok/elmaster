<?php

use yii\db\Migration;
use yii\db\Schema;

class m160426_201135_start extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING.' NOT NULL',
            'surname' => Schema::TYPE_STRING.' NOT NULL',
            'login' => $this->string(40)->notNull()->unique(),
            'password' => Schema::TYPE_STRING.' NOT NULL',
            'role' => Schema::TYPE_INTEGER.' NOT NULL',
        ], $tableOptions);


        $this->createTable('{{%post}}', [
            'id' => Schema::TYPE_PK,
            'text' => Schema::TYPE_TEXT . ' NOT NULL',
            'date' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
            'author_id' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->createIndex('FK_post_author', '{{%post}}', 'author_id');
        $this->addForeignKey(
            'FK_post_author', '{{%post}}', 'author_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE'
        );

        $this->insert('{{%user}}',array(
            'name' => 'Егор',
            'surname' => 'Шалаев',
            'login' => 'admin',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'role' => app\models\User::ROLE_ADMIN,
        ));

    }

    public function down()
    {
        $this->dropTable('{{%post}}');
        $this->dropTable('{{%user}}');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
